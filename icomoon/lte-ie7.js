/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-gplus' : '&#xe000;',
			'icon-facebook' : '&#xe001;',
			'icon-twitter' : '&#xe002;',
			'icon-pinterest' : '&#xe003;',
			'icon-picture' : '&#xe004;',
			'icon-folder' : '&#xe005;',
			'icon-history' : '&#xe006;',
			'icon-drawer' : '&#xe007;',
			'icon-user' : '&#xe008;',
			'icon-stats-up' : '&#xe009;',
			'icon-twitter-2' : '&#xe00a;',
			'icon-facebook-2' : '&#xe00b;',
			'icon-home' : '&#xe00c;',
			'icon-newspaper' : '&#xe00d;',
			'icon-pencil' : '&#xe00e;',
			'icon-tag' : '&#xe00f;',
			'icon-basket' : '&#xe010;',
			'icon-mail' : '&#xe011;',
			'icon-phone' : '&#xe012;',
			'icon-mobile' : '&#xe013;',
			'icon-comments' : '&#xe014;',
			'icon-cube' : '&#xe015;',
			'icon-location' : '&#xe016;',
			'icon-mail-2' : '&#xe017;',
			'icon-checkbox' : '&#xe018;',
			'icon-link' : '&#xe019;',
			'icon-clipboard' : '&#xe01a;',
			'icon-clock' : '&#xe01b;',
			'icon-calendar' : '&#xe01c;',
			'icon-pencil-2' : '&#xe01d;',
			'icon-copy' : '&#xe01e;',
			'icon-cabinet' : '&#xe01f;',
			'icon-screen' : '&#xe020;',
			'icon-laptop' : '&#xe021;',
			'icon-mouse' : '&#xe022;',
			'icon-printer' : '&#xe023;',
			'icon-drawer-2' : '&#xe024;',
			'icon-box-add' : '&#xe025;',
			'icon-box' : '&#xe026;',
			'icon-box-remove' : '&#xe027;',
			'icon-download' : '&#xe028;',
			'icon-upload' : '&#xe029;',
			'icon-comments-2' : '&#xe02a;',
			'icon-bars' : '&#xe02b;',
			'icon-pie' : '&#xe02c;',
			'icon-briefcase' : '&#xe02d;',
			'icon-attachment' : '&#xe02e;',
			'icon-bookmark' : '&#xe02f;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};